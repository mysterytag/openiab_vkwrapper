package com.example.vksuspiciousactivity;
import android.support.annotation.Nullable;
import android.util.Log;
import android.os.Bundle;
import com.vk.sdk.*;
import com.vk.sdk.api.*;
import com.unity3d.player.*;
import com.localytics.android.unity.LocalyticsApplication;
import com.vk.sdk.api.VKApi.*;
import com.vk.sdk.api.VKRequest.*;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKParameters;

public class Wrapper extends LocalyticsApplication {
    VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {
        @Override
        public void onVKAccessTokenChanged(@Nullable VKAccessToken oldToken, @Nullable VKAccessToken newToken) {
            if (newToken == null) {
                Log.d("Unity", "VKSDK TOKEN KLSAFLKAJ " + newToken);
//                Intent intent = new Intent(Application.this, LoginActivity.class);
//                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(intent);
//                Toast.makeText(Application.this, "AccessToken invalidated", Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("Unity", "VKSDK WRAPPER INIT1 " + this);
        vkAccessTokenTracker.startTracking();
        VKSdk.initialize(this);
        Log.d("Unity", "VKSDK WRAPPER INIT2");
    }
}
