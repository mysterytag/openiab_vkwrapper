package com.example.vksuspiciousactivity;
import android.app.*;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.Nullable;
import android.util.Log;
import android.os.Bundle;
import android.content.Intent;

import com.localytics.android.Localytics;
import com.vk.sdk.*;
import com.vk.sdk.api.model.VKApiUser;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.payments.*;
import com.vk.sdk.api.*;
import com.unity3d.player.*;
import com.vk.sdk.api.model.VKScopes;


public class VkTokenActivity extends UnityPlayerActivity {

    static VkTokenActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try
        {
            ApplicationInfo appInfo = getPackageManager().getApplicationInfo(getPackageName(), PackageManager.GET_META_DATA);

            String pushId = appInfo.metaData.getString("LOCALYTICS_PUSH_PROJECT_ID");
            Localytics.registerPush(pushId.trim());

        }
        catch (PackageManager.NameNotFoundException e)
        {
            Log.d("Unity", "localytics error"+ e.getMessage());
        }
        instance = this;
        Log.d("Unity", "VKSDK VkTokenActivity INIT");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.d("Unity", "VKSDK onActivityResult called " + requestCode + " " + resultCode);

        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {

                SendMessage("login", res.accessToken);
            }
            @Override
            public void onError(VKError error) {
                SendMessage("login", "error " + error.errorMessage + " " + error.errorCode);
            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public static void Login() {
        Log.d("Unity", "VKSDK login called");
        VKSdk.login(instance, VKScopes.WALL, VKScopes.GROUPS, VKScopes.FRIENDS);
    }

    private void SendNotificationImpl(int id, float delay, String title, String message) {
        Resources res = getResources();
        int icon = res.getIdentifier("app_icon", "drawable", getPackageName());
        Bitmap iconLarge = BitmapFactory.decodeResource(res, icon);

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        android.app.Notification notification = new Notification.Builder(this)
                .setContentText(message)
                .setSmallIcon(icon)
                .setLargeIcon(iconLarge)
                .setWhen(System.currentTimeMillis() + (long)delay * 1000)
                .build();

        Intent notificationIntent = new Intent(this, VkTokenActivity.class);
        notificationIntent.putExtra("notif_id", "5100");

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        notification.setLatestEventInfo(VkTokenActivity.this, title, message, pendingIntent);
        notificationManager.notify(1, notification);
    }

    public static void SendNotification(int id, float delay, String title, String message) {
        instance.SendNotificationImpl(id, delay, title, message);
        Log.d("Unity", "SendNotification " + title + ", " + message);
    }

    public  static  void GetFriendList() {
        Log.d("Unity", "VKSDK GetFriendList");
        try {
            VKRequest request1 = new VKRequest("friends.get", VKParameters.from(VKApiConst.FIELDS, "name, photo_100,sex"));
            VKRequest request2 = new VKRequest("friends.getAppUsers", VKParameters.from());

            request1.executeWithListener(new VKRequest.VKRequestListener() {
                @Override
                public void onComplete(VKResponse response) {
                    SendMessage("friends", response.responseString);
                }

                @Override
                public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                    SendMessage("friends", "timeout");
                }

                @Override
                public void onError(VKError error) {
                    SendMessage("friends", "error " + error.errorMessage + error.errorCode);
                }
            });

            request2.executeWithListener(new VKRequest.VKRequestListener() {
                @Override
                public void onComplete(VKResponse response) {
                    SendMessage("friends_app", response.responseString);
                }

                @Override
                public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                    SendMessage("friends_app", "timeout");
                }

                @Override
                public void onError(VKError error) {
                    SendMessage("friends_app", "error " + error.errorMessage + error.errorCode);
                }
            });
        } catch (Exception e) {
            Log.d("Unity", "## EXCEPTION VKSDK GetFriendList " + e.getMessage());
        }
    }

    public static void RequestUserOrigin()  {
        Log.d("Unity", "VKSDK RequestUserOrigin");
        //UnityPlayer.UnitySendMessage("VKWrapper", "SetUserOrigin", "-1");

        VKSdk.requestUserState(instance, new VKPaymentsCallback() {
            @Override
            public void onUserState(final boolean userIsVk) {
                Log.d("Unity", "VKSDK onUserState");
                SendMessage("UserOriginResponse", userIsVk ? "1" : "0");
            }
        });
    }

    public  static  void  GetUserProfile() {
        Log.d("Unity", "VKSDK GetUserProfile");
        VKRequest request = VKApi.users().get(VKParameters.from(VKApiConst.FIELDS, "first_name, last_name, bdate, country, sex"));
        Log.d("Unity", request.toString());
        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                SendMessage("user_profile", response.responseString);
            }

            @Override
            public void onError(VKError error) {
                SendMessage("user_profile", "error " + error.errorMessage + error.errorCode);
            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                SendMessage("user_profile", "timeout");
            }
        });
    }

    public static void WallPost(String message) {
        VKRequest request = VKApi.wall().post(VKParameters.from(VKApiConst.MESSAGE, message));

        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                Log.d("Unity", "VKSDK PostAchievement Success");
                SendMessage("wall_post_result", "success");
            }

            @Override
            public void onError(VKError error) {
                Log.d("Unity", "VKSDK PostAchievement Error");
            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                Log.d("Unity", "VKSDK PostAchievement Timeout");
            }
        });
    }


    public static void PostAchievement(String message, String info) {
        VKRequest request = VKApi.wall().post(VKParameters.from(VKApiConst.MESSAGE, message, VKApiConst.ATTACHMENTS, info));

        request.executeWithListener(new VKRequest.VKRequestListener() {
            @Override
            public void onComplete(VKResponse response) {
                Log.d("Unity", "VKSDK PostAchievement Success");
            }

            @Override
            public void onError(VKError error) {
                Log.d("Unity", "VKSDK PostAchievement Error");
            }

            @Override
            public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                Log.d("Unity", "VKSDK PostAchievement Timeout");
            }
        });
    }

    public  static  void  SendMessage(final String method, final String message) {
        instance.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                UnityPlayer.UnitySendMessage("callbacks","Event", method+"~"+ message);
            }
        });
    }

    public static void JoinGroup(String id) {
        Log.d("Unity", "VKSDK JoinGroup " + id);

        try {
            VKRequest request = VKApi.groups().join(VKParameters.from(VKApiConst.GROUP_ID, id));
            request.executeWithListener(new VKRequest.VKRequestListener() {
                @Override
                public void onComplete(VKResponse response) {
                    SendMessage("join_group", response.responseString);
                }

                @Override
                public void onError(VKError error) {
                    SendMessage("join_group", "error " + error.errorMessage + error.errorCode);
                }

                @Override
                public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                    SendMessage("join_group", "timeout");
                }
            });
        } catch (Exception e) {
            Log.d("Unity", "## EXCEPTION VKSDK JoinGroup " + e.getMessage());
        }
    }

    public static  void  InviteFriend(String id) {
        VKRequest request =  new VKRequest("apps.sendRequest", VKParameters.from(VKApiConst.USER_ID, id, "type", "invite", VKApiConst.OWNER_ID, "5557641"));
        try {
            request.executeWithListener(new VKRequest.VKRequestListener() {
                @Override
                public void onComplete(VKResponse response) {
                    SendMessage("invite_friend", response.responseString);
                }

                @Override
                public void onError(VKError error) {
                    SendMessage("invite_friend", "error " + error.errorMessage + error.errorCode);
                }

                @Override
                public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                    SendMessage("invite_friend", "timeout");
                }
            });
        } catch (Exception e) {
            Log.d("Unity", "## EXCEPTION VKSDK InviteFriend " + e.getMessage());
        }
    }

    public  static boolean IsLoggedIn() {
        Log.d("suc", "cess");
        return VKSdk.isLoggedIn();
    }

    public  static void Logout() {
        VKSdk.logout();
    }

}
